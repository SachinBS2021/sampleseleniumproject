package com.screenshot;

import java.io.File;
import java.io.IOException;
import java.util.Timer;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.utility.TakeScreenshot;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ScreenShot {

	WebDriver driver;
	
	@BeforeMethod
	public void setup()
	{
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://google.com/");
		
	}
	
	@Test
	public void testcase1() 
	{
		//new WebDriverWait(driver, Time.FromSeconds(20)).until(ExpectedConditions.elementToBeClickable(By.cssSelector("#input[type='search']"))).SendKeys("APPLES");
		driver.findElement(By.cssSelector("#input[type='search']")).sendKeys("screenshot Demo");
		TakeScreenshot.screenshot(driver, "Screenshot2");
	}
	
	@AfterMethod
	public void tearDown(ITestResult result)
	{
		if(result.FAILURE==result.getStatus())
		{
			TakeScreenshot.screenshot(driver, result.getName());
		}
		driver.quit();
	}
}
